<?php
    $id=$_REQUEST['id'];
    include('conexion.php');
    $query = "SELECT * FROM tours WHERE id='$id' ";
    $resultado = $con -> query($query);
    $row=$resultado->fetch_assoc();
?>

<div class="container">
  <h2>Información del Tour</h2>

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Incluido</a></li>
    <li><a data-toggle="tab" href="#menu1">No Incluido</a></li>
    <li><a data-toggle="tab" href="#menu2">Adicional</a></li>
  </ul>
  <div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <h3>Servicios Incluidos</h3>
    <p><?php echo $row['incluye']; ?></p>
  </div>
  <div id="menu1" class="tab-pane fade">
    <h3>Servicios no Incluidos</h3>
    <p><?php echo $row['noIncluye']; ?></p>
  </div>
  <div id="menu2" class="tab-pane fade">
    <h3>Servicios Adicionales</h3>
    <p><?php echo $row['adicional']; ?></p>
  </div>
</div>
</div>
