<?php include('conexion.php'); ?>
<?php

    session_start();

    $id=$_SESSION['id'];
    $query = "SELECT * FROM transporte WHERE id='$id' ";
    $resultado = $con -> query($query);
    $row=$resultado->fetch_assoc();

    $origen = $_GET['car'];
    $_SESSION['origen'] = $origen;
    $destino = $_GET['destino'];
    $_SESSION['destino'] = $destino;
    $pasajeros = $_GET['pasajeros'];
    $_SESSION['pasajeros'] = $pasajeros;
    $silla = $_GET['silla'];
    $_SESSION['silla'] = $silla;
    $fechaS = $_GET['datepicker'];
    $_SESSION['fechaS'] = $fechaS;
    $horaS = $_GET['horaS'];
    $_SESSION['horaS'] = $horaS;
    $_SESSION['tipoviaje'] = "sencillo";
    $aerolineaS = $_GET['aerolineaS'];
    $_SESSION['aerolineaS'] = $aerolineaS;
    $vueloS = $_GET['vueloS'];
    $_SESSION['vueloS'] = $vueloS;

    $_SESSION['total'] = $row['sencilloD']." USD";

?>
<!-- header include Start -->
<?php include("headerCatalogo.php")?>
<!-- header start complement-->
  <div class="templatemo_headerimage">
    <div class="flexslider">
      <ul class="slides">
        <li><img src="images/<?php echo $row['foto2']; ?>"></li>
      </ul>
    </div>
  </div>
  <div class="slider-caption">
    <div class="templatemo_homewrapper">
      <div class="templatemo_hometitle"><img src="images/logos/logo3.png" alt="Logo empresa" width="125" height="125" align="bottom"></div>
      <div class="templatemo_hometext"><?php echo $row['descripcion']; ?></div>
    </div>
  </div>
</div>
<!-- header end complement-->
<!-- header include END-->
<div class="clear"></div>
<!--Detalle Transporte start-->
<br>
<div class="container">
  <div class="row">
       <div class="col-md-5 col-sm-6">
            <div class="about-info">
              <div class="section-title">
                    <p><h3>Salida:</h3></p>
                    <p><strong>Origin : </strong><?php echo "Hotel ".$_SESSION['destino']; ?></p>
                    <p><strong>Destination : </strong><?php echo $_SESSION['origen'];?></p>
                    <p><strong> Departure date: </strong><?php echo $_SESSION['fechaS']?></p>
                    <p><strong> Departure time: </strong><?php echo $_SESSION['horaS']?></p>
                    <p><strong> Departure Airline: </strong><?php echo $_SESSION['aerolineaS']?></p>
                    <p><strong> Flight number: </strong><?php echo $_SESSION['vueloS'];?></p>
                    <p><strong> Requires baby chair: </strong><?php echo $_SESSION['silla'];?>
                    <p><strong> Total: </strong><?php echo "$".$_SESSION['total'];?></p>
                   <span class="line-bar"></span>
              </div>
            </div>
       </div>

       <div class="col-md-3 col-sm-6">
            <div class="about-info skill-thumb">

            </div>
       </div>

       <div class="col-md-4 col-sm-12">
            <div class="about-image">
                 <img src="images/<?php echo $row['fotoC']; ?>" class="figure-img img-fluid rounded img-thumbnail" border="0" width="400" height="400">
            </div>
       </div>
  </div>
</div>
<!--Detalle Transporte End-->
<div class="clear"></div>
<!--Start Form datos personales-->
<?php include("formsPersonalTransporteS.php")?>
<!--Start Form datos personales End-->

<?php include('footer.php'); ?>
