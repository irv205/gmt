<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker2" ).datepicker({minDate: 0, showAnim: "fold"});
  } );
  </script>
</head>
<body>

<label>Date</label>
<input type="text" class="form-control" id="datepicker2" name="datepicker2">


</body>
</html>
