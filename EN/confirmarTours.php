<?php include('conexion.php'); ?>
<?php

    session_start();

    $id=$_SESSION['id'];
    $query = "SELECT * FROM tours WHERE id='$id' ";
    $resultado = $con -> query($query);
    $row=$resultado->fetch_assoc();

    $hotel = $_GET['car'];
    $_SESSION['hotel']=$hotel;
    $date = $_GET['datepicker'];
    $_SESSION['fecha']=$date;
    $horario = $_GET['horario'];
    $_SESSION['horario']=$horario;
    $adultos = $_GET['adulto'];
    $_SESSION['adultos']=$adultos;
    $menores = $_GET['menor'];
    $_SESSION['menores']=$menores;

    $precioA = $row['precioD'];
    $precioN = $row['precioDN'];
    $precioA = $precioA*$adultos;
    $precioN = $precioN*$menores;
    $total = $precioA+$precioN;
    $_SESSION['total']=$total;

?>
<!-- header include Start -->
<?php include("headerCatalogo.php")?>
<!-- header start complement-->
  <div class="templatemo_headerimage">
    <div class="flexslider">
      <ul class="slides">
        <li><img src="images/<?php echo $row['foto2']; ?>"></li>
      </ul>
    </div>
  </div>
  <div class="slider-caption">
    <div class="templatemo_homewrapper">
      <div class="templatemo_hometitle"><img src="images/logos/logo3.png" alt="Logo empresa" width="125" height="125" align="bottom"></div>
      <div class="templatemo_hometext"><?php echo $row['descripcionL']; ?></div>
    </div>
  </div>
</div>
<!-- header end complement-->
<!-- header include END-->
<div class="clear"></div>
<!--Detalle Tours start-->
<br>
<div class="container">
  <div class="row">

       <div class="col-md-5 col-sm-6">
            <div class="about-info">
                 <div class="section-title">
                      <h2><?php echo $row['nombre']; ?></h2>
                      <span class="line-bar"></span>
                 </div>
                 <p><?php echo "Hotel: ".$_SESSION['hotel']; ?></p>
                 <p><?php echo "Fecha del tour: ".$_SESSION['fecha']; ?></p>
                 <p><?php echo "Hora del Tour: ".$_SESSION['horario']; ?></p>
                 <p><?php echo "Adultos: ".$_SESSION['adultos']; ?></p>
                 <p><?php if($menores != 0) echo "Niños: ".$_SESSION['menores']; ?></p>
                 <p>Total a pagar:<?php echo " $".$_SESSION['total']." USD"; ?></p>

            </div>
       </div>

       <div class="col-md-3 col-sm-6">
            <div class="about-info skill-thumb">

            </div>
       </div>

       <div class="col-md-4 col-sm-12">
            <div class="about-image">
                 <img src="images/<?php echo $row['fotoC']; ?>" class="figure-img img-fluid rounded img-thumbnail" border="0" width="400" height="400">
            </div>
       </div>

  </div>
</div>
<!--Detalle Tours End-->
<div class="clear"></div>
<!--Start Form datos personales-->
<?php include("formsPersonal.php")?>
<!--Start Form datos personales End-->
<div class="clear"></div>

<?php include('footer.php'); ?>
