<?php include('conexion.php'); ?>
<?php

  session_start();

    $id=$_REQUEST['id'];
    $_SESSION['id']=$id;
    $query = "SELECT * FROM tours WHERE id='$id' ";
    $resultado = $con -> query($query);
    $row=$resultado->fetch_assoc();

?>
<!-- header include -->
<?php include("headerCatalogo.php")?>
<!-- header start complement-->
  <div class="templatemo_headerimage">
    <div class="flexslider">
      <ul class="slides">
        <li><img src="images/<?php echo $row['foto2']; ?>"></li>
      </ul>
    </div>
  </div>
  <div class="slider-caption">
    <div class="templatemo_homewrapper">
      <div class="templatemo_hometitle"><img src="images/logos/logo3.png" alt="Logo empresa" width="125" height="125" align="bottom"></div>
      <div class="templatemo_hometext"><?php echo $row['descripcionL']; ?></div>
    </div>
  </div>
</div>
<!-- header end complement-->
<div class="clear"></div>
<!--Detalle Tours start-->
<br>
<div class="container">
  <div class="row">

       <div class="col-md-5 col-sm-6">
            <div class="about-info">
                 <div class="section-title">
                      <h2><?php echo $row['nombre']; ?></h2>
                      <span class="line-bar"></span>
                 </div>
                 <h4>Adult: $<?php echo $row['precioD']; ?> USD</h4>
                 <h4>Children: $<?php echo $row['precioDN']; ?> USD</h4>
            </div>
       </div>

       <div class="col-md-3 col-sm-6">
            <div class="about-info skill-thumb">


            </div>
       </div>

       <div class="col-md-4 col-sm-12">
            <div class="about-image">
                 <img src="images/<?php echo $row['fotoC']; ?>" class="img-responsive" alt="">
            </div>
       </div>

  </div>
</div>
<!--Detalle Tours End-->
<div class="clear"></div>
<!-- Tabs start-->
<?php include("tabs.php")?>
<!-- Tabs end-->
<div class="clear"></div>
<!--Form Start Cotizacion-->
<?php include("toursForm.php")?>
<!--Form end Cotizacion-->
<div class="clear"></div>

<?php include('footer.php'); ?>
