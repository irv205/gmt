<!-- service start -->
<div class="templatemo_servicewrapper" id="templatemo_service_page">
  <div class="container">
    <div class="row">
      <h1><a href="toursCatalogo.php">Tours</a></h1>
      <div class="col-md-12 templatemo_marginbot">En <strong>GM Transportation & Tours</strong> We invite you to know our exclusive collection of Tours.[Cambiar Textos]</div>
      <div class="col-md-3 col-sm-6">
        <div class="templatemo_servicebox margin_bottom_1col margin_bottom_2col">
          <div class="templatemo_serviceicon"><span class="fa fa-plane"></span></div>
          <div class="templatemo_service_title">Travel wherever you want</div>
          <p>Morbi et nisi in augue accumsan imperdiet Morbi et nisi in augue accumsan imperdiet Morbi et nisi in augue accumsan imperdiet.[Cambiar Textos]</p>
        </div>
      </div>

      <div class="col-md-3 col-sm-6">
        <div class="templatemo_servicebox margin_bottom_1col margin_bottom_2col">
          <div class="templatemo_serviceicon"><span class="fa fa-calendar"></span></div>
          <div class="templatemo_service_title">Reserve with us</div>
          <p>Morbi et nisi in augue accumsan imperdiet Morbi et nisi in augue accumsan imperdiet Morbi et nisi in augue accumsan imperdiet.[Cambiar Textos]</p>
        </div>
      </div>

      <div class="col-md-3 col-sm-6">
        <div class="templatemo_servicebox margin_bottom_1col">
          <div class="templatemo_serviceicon"><span class="fa fa-building"></span></div>
          <div class="templatemo_service_title">It doesn't matter where you stay We</div>
          <p>Morbi et nisi in augue accumsan imperdiet Morbi et nisi in augue accumsan imperdiet Morbi et nisi in augue accumsan imperdiet.[Cambiar Textos]</p>
        </div>
      </div>

      <div class="col-md-3 col-sm-6">
        <div class="templatemo_servicebox">
          <div class="templatemo_serviceicon"><span class="fa fa-automobile"></span></div>
          <div class="templatemo_service_title">we do everything for you</div>
          <p>Morbi et nisi in augue accumsan imperdiet Morbi et nisi in augue accumsan imperdiet Morbi et nisi in augue accumsan imperdiet.[Cambiar Textos]</p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- service end -->

<!-- service start -->
<div class="templatemo_servicewrapper" id="templatemo_service_page">
  <div class="container">
    <div class="row">
      <h1><a href="transporteCatalogo.php">Transfers</a></h1>
      <div class="col-md-12 templatemo_marginbot">En <strong>GM Transportation & Tours</strong> We offer you the following transport services.[Cambiar Textos]</div>
      <div class="col-md-3 col-sm-6">
        <div class="templatemo_servicebox margin_bottom_1col margin_bottom_2col">
          <div class="templatemo_serviceicon"><span class="fa fa-plane"></span></div>
          <div class="templatemo_service_title">Airport - Hotel</div>
          <p>Traslados del Aeropuerto Internacional de Cancún a cualquier hotel de la zona hotelera.[Cambiar Textos]</p>
        </div>
      </div>

      <div class="col-md-3 col-sm-6">
        <div class="templatemo_servicebox margin_bottom_1col margin_bottom_2col">
          <div class="templatemo_serviceicon"><span class="fa fa-building-o"></span></div>
          <div class="templatemo_service_title">Hotel - Airport</div>
          <p>Te trasladamos de tu Hotel o cualquier zona en Cancún, al Aeropuerto Internacional de Cancún.[Cambiar Textos]</p>
        </div>
      </div>

      <div class="col-md-3 col-sm-6">
        <div class="templatemo_servicebox margin_bottom_1col">
          <div class="templatemo_serviceicon"><span class="fa fa-building"></span></div>
          <div class="templatemo_service_title">Hotel - Hotel</div>
          <p>¿Te vas a cambiar de hotel? no hay problema, te recogemos en tu hotel y te llevamos a otro, en una camioneta privada, con todas las comodidades.[Cambiar Textos]</p>
        </div>
      </div>

      <div class="col-md-3 col-sm-6">
        <div class="templatemo_servicebox">
          <div class="templatemo_serviceicon"><span class="fa fa-automobile"></span></div>
          <div class="templatemo_service_title">Group Transportation</div>
          <p>Somos especialistas en Traslados para Grupos de mas de 10 personas</p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- service end -->
