<?php include('conexion.php'); ?>
<?php

  session_start();

    $id=$_REQUEST['id'];
    $_SESSION['id']=$id;
    $query = "SELECT * FROM transporte WHERE id='$id' ";
    $resultado = $con -> query($query);
    $row=$resultado->fetch_assoc();
?>
<!-- header include -->
<?php include("headerCatalogo.php")?>
<!-- header start complement-->
  <div class="templatemo_headerimage">
    <div class="flexslider">
      <ul class="slides">
        <li><img src="images/<?php echo $row['foto2']; ?>"></li>
      </ul>
    </div>
  </div>
  <div class="slider-caption">
    <div class="templatemo_homewrapper">
      <div class="templatemo_hometitle"><img src="images/logos/logo3.png" alt="Logo empresa" width="125" height="125" align="bottom"></div>
      <div class="templatemo_hometext"><?php echo $row['descripcion']; ?></div>
    </div>
  </div>
</div>
<!-- header end complement-->
<div class="clear"></div>
<!--Detalle transporte start-->
<br>
<div class="container">
  <div class="row">

       <div class="col-md-5 col-sm-6">
            <div class="about-info">
                 <div class="section-title">
                      <h2><?php echo $row['nombre']; ?></h2>
                      <span class="line-bar"></span>
                 </div>
                 <h4>Viaje Sencillo: $<?php echo $row['sencilloD']; ?> USD</h4>
                 <h4>Viaje Redondo: $<?php echo $row['redondoD']; ?> USD</h4>
            </div>
       </div>

       <div class="col-md-3 col-sm-6">
            <div class="about-info skill-thumb">

            </div>
       </div>

       <div class="col-md-4 col-sm-12">
            <div class="about-image">
                 <img src="images/<?php echo $row['fotoC']; ?>" class="img-responsive" alt="">
            </div>
       </div>

  </div>
</div>
<!--Detalle transporte End-->

<!--Start Tabs-->
<div class="container">
  <h2>Transport information</h2>

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Included</a></li>
    <li><a data-toggle="tab" href="#menu1">No Included</a></li>
    <li><a data-toggle="tab" href="#menu2">additional</a></li>
  </ul>
  <div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <h3>Services Included</h3>
    <p>LO QUE SE DEBE INCLUIR</p>
  </div>
  <div id="menu1" class="tab-pane fade">
    <h3>Services No Included</h3>
    <p>LO QUE NO SE DEBE INCLUIR</p>
  </div>
  <div id="menu2" class="tab-pane fade">
    <h3>Services additional</h3>
    <p>LO QUE PUEDES AÑADIR</p>
  </div>
</div>
</div>
<!--End Tabs-->
<div class="clear"></div>
<!--Form Start Cotizacion-->
<?php include("transporteType.php")?>
<!--Form end Cotizacion-->
<div class="clear"></div>
<?php include('footer.php'); ?>
