<!DOCTYPE html>

<html lang="es">
<head>

<title>Transportation & Tours</title>
<link rel="icon" type="image/ico" href="images/logos/logo1.png" />
<meta name="keywords" content="">
<meta name="description" content="">
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700,800' rel='stylesheet' type='text/css'>
<!-- Style Sheets -->
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/templatemo_misc.css">
<link rel="stylesheet" href="css/templatemo_style.css">

<!-- JavaScripts -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap-dropdown.js"></script>
<script src="js/bootstrap-collapse.js"></script>
<script src="js/bootstrap-tab.js"></script>
<script src="js/jquery.singlePageNav.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/custom.js"></script>
<script src="js/jquery.lightbox.js"></script>
<script src="js/templatemo_custom.js"></script>
<script src="js/responsiveCarousel.min.js"></script>

<!--script tabs-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

</head>

<body>
<!-- header start -->
<div>
  <div class="templatemo_topbar">
    <div class="container">
      <div class="row">
        <div class="templatemo_titlewrapper"><img src="images/templatemo.png" alt="logo background"></div>
        <div class="clear"></div>
        <a href="index.php"><div style="position: relative;" class="templatemo_titlewrappersmall">Transportation & Tours</div></a>
        <nav class="navbar navbar-default templatemo_menu" role="navigation">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="top-menu">
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li><a class="menu" href="index.php">Home</a></li>
                  <li><a class="menu" href="serviciosCatalogo.php">Services</a></li>
                  <li><a class="menu" href="toursCatalogo.php">Tours</a></li>
                  <li><a class="menu" href="transporteCatalogo.php">Transfers</a></li>
                  <li><a class="menu" href="contactoCatalogo.php">Contact</a></li>
                </ul>
              </div>
            </div>
            <!-- /.navbar-collapse -->
          </div>
          <!-- /.container-fluid -->
        </nav>
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <div class="templatemo_headerimage">
    <div class="flexslider">
      <ul class="slides">
        <li><img src="images/slider/1.jpg" alt="Slide 1"></li>
        <li><img src="images/slider/2.jpg" alt="Slide 2"></li>
        <li><img src="images/slider/3.jpg" alt="Slide 3"></li>
      </ul>
    </div>
  </div>
  <div class="slider-caption">
    <div class="templatemo_homewrapper">
      <div class="templatemo_hometitle"><img src="images/logos/logo3.png" alt="Logo empresa" width="125" height="125" align="bottom"></div>
      <div class="templatemo_hometext"><strong>GM Transportation & Tours</strong> It is a company committed to its customers, we seek satisfaction committed to give the best, the services we offer go hand in hand with a quality standard.[Cambiar Textos]</div>
      <div class="templatemo_hometitle"><a title="Logo" href="../"><img src="images/mexico.svg" alt="Logo" width="25" height="25" align="bottom"></a></div>
    </div>
  </div>
</div>
<!-- header end -->
