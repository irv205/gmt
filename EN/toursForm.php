<?php include('conexion.php'); ?>
<?php

    $id=$_SESSION['id'];
    $query = "SELECT * FROM tours WHERE id='$id' ";
    $resultado = $con -> query($query);
    $row=$resultado->fetch_assoc();

?>
<div class="container">
    <div class="col-md-12 col-sm-12">
      <div class="templatemo_servicebox margin_bottom_1col margin_bottom_2col">

        <div class="templatemo-content-widget white-bg">
                    <h2 class="margin-bottom-10">Quotation: </h2>

                    <form action="confirmarTours.php" class="templatemo-login-form" method="get" enctype="multipart/form-data">
                      <div class="row form-group">
                        <div class="col-lg-6 col-md-6 form-group" class="ui-widget">
                            <label for="tags">Hotel</label>
                            <?php include("buscador.php")?>
                        </div>

                        <div class="col-lg-4 col-md-4 form-group">
                            <?php include("dateFormat.php")?>
                        </div>
                      </div>
                      <div class="row form-group">

                        <div class="row form-group">

                        </div>

                        <div class="col-lg-4 col-md-4 form-group">
                          <label class="control-label templatemo-block">schedule: </label>
                          <select name="horario" class="form-control" size="1">
                            <option value="7:00 AM">7:00 AM</option>
                            <option value="8:00 AM">8:00 AM</option>
                            <option value="9:00 AM">9:00 AM</option>
                            <option value="10:00 AM">10:00 AM</option>
                          </select>
                        </div>

                        </div>

                      <div class="row form-group">
                        <div class="col-lg-4 col-md-4 form-group">
                          <label class="control-label templatemo-block">Adult: </label>
                          <select name="adulto" class="form-control" size="1">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                          </select>
                        </div>

                        <div class="col-lg-4 col-md-4 form-group">
                          <label class="control-label templatemo-block">Children: </label>
                          <select name="menor" class="form-control" size="1">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                          </select>
                        </div>

                      </div>
                      <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary">Confirm</button>
                        <button type="reset" class="btn btn btn-danger">Clear</button>
                      </div>
                    </form>
                  </div>

      </div>
    </div>
</div>
