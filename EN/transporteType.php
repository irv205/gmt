<?php include('conexion.php'); ?>
<?php

    $id=$_SESSION['id'];
    $query = "SELECT * FROM transporte WHERE id='$id' ";
    $resultado = $con -> query($query);
    $row=$resultado->fetch_assoc();

?>

<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#tabs-1').hide();
    $('#tabs-2').hide();

    $('#enlace1').click(function(){
      $('#tabs-1').show();
      $('#tabs-2').hide();
    });

    $('#enlace2').click(function(){
      $('#tabs-2').show();
      $('#tabs-1').hide();
    });

  });
</script>

<div class="container">
    <div class="col-md-12 col-sm-12">
      <div class="templatemo_servicebox margin_bottom_1col margin_bottom_2col">

        <div class="templatemo-content-widget white-bg">
                    <h2 class="margin-bottom-10">Quotation: </h2>

                    <div class="row form-group">

                      <div class="row" >
                        <div class="col-md-6">
                          <div id="tabs">
                            <ul class="nav nav-pills">
                              <label>Type of trip:</label></br>
                            <li role="presentation"><a href="#tabs-1" id="enlace1">One way</a></li>
                            <li role="presentation"><a href="#tabs-2" id="enlace2">Round Trip</a></li>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div id="tabs-1">
                            <?php include("transporteFormS.php") ?>
                        </div>
                        </div>

                        <div class="col-md-12">
                          <div id="tabs-2">
                             <?php include("transporteFormR.php") ?>
                          </div>
                        </div>

                      </div>

                    </div>

       </div>

      </div>
    </div>
</div>
