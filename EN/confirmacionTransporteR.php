<?php include('conexion.php'); ?>
<?php

    session_start();

    $id=$_SESSION['id'];
    $query = "SELECT * FROM transporte WHERE id='$id' ";
    $resultado = $con -> query($query);
    $row=$resultado->fetch_assoc();

    $origen = $_GET['origen'];
    $_SESSION['origen'] = $origen;
    $destino = $_GET['carr'];
    $_SESSION['destino'] = $destino;
    $pasajeros = $_GET['pasajeros'];
    $_SESSION['pasajeros'] = $pasajeros;
    $silla = $_GET['silla'];
    $_SESSION['silla'] = $silla;
    $fechaLl = $_GET['datepicker1'];
    $_SESSION['fechaLl'] = $fechaLl;
    $horaLl = $_GET['horallegada'];
    $_SESSION['horaLl'] = $horaLl;
    $_SESSION['tipoviaje'] = "redondo";
    $aerolineaLl = $_GET['aerolineaLl'];
    $_SESSION['aerolineaLl'] = $aerolineaLl;
    $vueloLl = $_GET['vueloLl'];
    $_SESSION['vueloLl'] = $vueloLl;
    $fechaS = $_GET['datepicker2'];
    $_SESSION['fechaS'] = $fechaS;
    $horaS = $_GET['horaSalida'];
    $_SESSION['horaS'] = $horaS;
    $aerolineaS = $_GET['aerolineaS'];
    $_SESSION['aerolineaS'] = $aerolineaS;
    $vueloS = $_GET['vueloS'];
    $_SESSION['vueloS'] = $vueloS;

    $_SESSION['total'] = $row['redondoD']." USD";

?>
<!-- header include Start -->
<?php include("headerCatalogo.php")?>
<!-- header start complement-->
  <div class="templatemo_headerimage">
    <div class="flexslider">
      <ul class="slides">
        <li><img src="images/<?php echo $row['foto2']; ?>"></li>
      </ul>
    </div>
  </div>
  <div class="slider-caption">
    <div class="templatemo_homewrapper">
      <div class="templatemo_hometitle"><img src="images/logos/logo3.png" alt="Logo empresa" width="125" height="125" align="bottom"></div>
      <div class="templatemo_hometext"><?php echo $row['descripcion']; ?></div>
    </div>
  </div>
</div>
<!-- header end complement-->
<!-- header include END-->
<div class="clear"></div>
<!--Detalle Transporte start-->
<br>
<div class="container">
  <div class="row">
       <div class="col-md-4 col-sm-4">
            <div class="about-info">
                 <div class="section-title">
                       <p><h3>Arrival:</h3></p>
                       <p><strong> Origin : </strong><?php echo $_SESSION['origen']; ?></p>
                       <p><strong> Destination : </strong><?php echo "Hotel ".$_SESSION['destino'];?></p>
                       <p><strong> Arrival date: </strong><?php echo $_SESSION['fechaLl'];?></p>
                       <p><strong> Check In: </strong><?php echo $_SESSION['horaLl'];?></p>
                       <p><strong> Number of passengers: </strong><?php echo $_SESSION['pasajeros'];?></p>
                       <p><strong> Requires baby chair: </strong><?php echo $_SESSION['silla'];?>
                       <p><strong> Arrival Airline: </strong><?php echo $_SESSION['aerolineaLl']?></p>
                       <p><strong> Flight number: </strong><?php echo $_SESSION['vueloLl'];?></p>
                 </div>
            </div>
       </div>
       <div class="col-md-4 col-sm-4">
            <div class="about-info">
                 <div class="section-title">
                       <p><h3>Departure:</h3></p>
                       <p><strong> Origin : </strong><?php echo "Hotel ".$_SESSION['destino']; ?></p>
                       <p><strong> Destination : </strong><?php echo $_SESSION['origen'];?></p>
                       <p><strong> Departure date: </strong><?php echo $_SESSION['fechaS']?></p>
                       <p><strong> Departure time: </strong><?php echo $_SESSION['horaS']?></p>
                       <p><strong> Departure Airline: </strong><?php echo $_SESSION['aerolineaS']?></p>
                       <p><strong> Flight number: </strong><?php echo $_SESSION['vueloS'];?></p>
                       <p><strong> Total: </strong><?php echo "$".$_SESSION['total'];?></p>
                      <span class="line-bar"></span>
                 </div>
            </div>
       </div>

       <div class="col-md-3 col-sm-6">
            <div class="about-info skill-thumb">

            </div>
       </div>

       <div class="col-md-4 col-sm-12">
            <div class="about-image">
                 <img src="images/<?php echo $row['fotoC']; ?>" class="figure-img img-fluid rounded img-thumbnail" border="0" width="500" height="500">
            </div>
       </div>

  </div>
</div>
<!--Detalle Transporte End-->
<div class="clear"></div>
<!--Start Form datos personales-->
<?php include("formsPersonalTransporte.php")?>
<!--Start Form datos personales End-->
<div class="clear"></div>

<?php include('footer.php'); ?>
