<!-- Tours start -->
<div class="templatemo_workwrapper">
  <div class="container">
    <div class="row">
      <h1><a href="toursCatalogo.php">Tours</a></h1>
      <div class="col-md-12 templatemo_workmargin">Te invitamos a conocer nuestra exclusiva colección de Tours en Cancún.</div>
    </div>
  </div>
  <div>

<!--Start Catalogo de Tours -->
  <?php
      include('conexion.php');
      $query = "SELECT * FROM tours";
      $resultado = $con -> query($query);
      while($row=$resultado->fetch_assoc())
      {
  ?>
      <div class="templatemo_workbox">
        <a href="toursDetalle.php?id=<?php echo $row['id'];?>">
        <div class="gallery-item">
            <img src="images/<?php echo $row['fotoC']; ?>" class="figure-img img-fluid rounded img-thumbnail" alt="Imagen no disponible" border="0">
          <div class="overlay">
            <div class="templatemo_worktitle"><?php echo $row['nombre']; ?></div>
            <div class="templatemo_workdes"><?php echo $row['descripcionC']; ?></div>
          </div>
        </div>
        </a>
      </div>
  <?php
      }
  ?>
<!--End Catalogo de Tours -->

  </div>
</div>
<!--Tours end-->
