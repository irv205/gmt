<!-- traslados start -->
<div class="templatemo_team_wrapper">
  <div class="container">
    <div class="row">
      <h1><a href="traslados.php">Traslados</a></h1>
      <div class="col-md-12 templatemo_workmargin">Usted no perderá tiempo. Una vez que se encuentre en el aeropuerto, nuestro personal estará esperando, quien sin intermediarios lo llevará a abordar su unidad reservada para el traslado. [Cambiar Textos]</div>

      <?php
          include('conexion.php');
          $query = "SELECT * FROM transporte";
          $resultado = $con -> query($query);
          while($row=$resultado->fetch_assoc())
          {
      ?>
          <div class="templatemo_workbox">
            <a href="transporteDetalle.php?id=<?php echo $row['id'];?>">
            <div class="gallery-item">
                <img src="images/<?php echo $row['fotoC']; ?>" class="figure-img img-fluid rounded img-thumbnail" alt="Imagen no disponible" border="0">
              <div class="overlay">
                <div class="templatemo_worktitle"><?php echo $row['nombre']; ?></div>
                <div class="templatemo_workdes"><?php echo $row['descripcion']; ?></div>
              </div>
            </div>
            </a>
          </div>
      <?php
          }
      ?>

    </div>
  </div>
</div>
<!-- traslados end -->
